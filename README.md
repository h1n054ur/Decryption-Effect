<div align="center">
  <img width="20%" src="https://user-images.githubusercontent.com/65507003/118815025-779a6600-b8b9-11eb-8de5-7d2ed15be180.png">
  </div>

# Caesar Cipher Decoder + Decryption Effect

 <div align="left">
  <img width="500"  src="https://user-images.githubusercontent.com/65507003/118805623-cc84af00-b8ae-11eb-9732-7d5f66e76bfe.gif" alt="animated">
</div>

Works for linux and windows

This is Caesar Cipher (4 shift) decoder with a little decryption effect.

You can also Encrypt the input by changing '-i' to '+i'.

<div>
  <h3>Thanks to h1no for the command line arguments!</h3>
  </div>
